import React from "react";
import {Route} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Main from "./containers/Main/Main";

const App = () => (
    <Layout>
            <Route path="/" component={Main} />
    </Layout>
);

export default App;
