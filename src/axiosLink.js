import axios from "axios";

const axiosLink = axios.create({
    baseURL: 'http://api.tvmaze.com/search/'
});

export default axiosLink;