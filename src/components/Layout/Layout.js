import React from "react";
import './Layout.css';
import {Link} from "react-router-dom";

const Header = props => {
    return (
        <>
            <header className="header">
                <div className="logo"><Link to="/" className="logo-link">TV Shows</Link></div>
            </header>
            <div className="container">
                {props.children}
            </div>
        </>
    );
};

export default Header;