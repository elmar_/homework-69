import React from 'react';
import './Titles.css';
import {useDispatch, useSelector} from "react-redux";
import {addShow} from "../../store/actions";

const Titles = ({props}) => {
    const dispatch = useDispatch();
    const shows = useSelector(state => state.shows);

    const setId = show => {
        dispatch(addShow(show, show.id));
        props.history.push('/show/' + show.id);
    }
    return shows.length > 0 && (
        <ul className="list">
            {shows.map(show => <li key={show.show.id} onClick={() => setId(show.show)}>{show.show.name}</li>)}
        </ul>
    );
};

export default Titles;