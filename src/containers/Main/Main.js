import React from 'react';
import './Main.css';
import {useDispatch, useSelector} from "react-redux";
import {findShows, inpValue} from "../../store/actions";
import axiosLink from "../../axiosLink";
import Titles from "../../components/Titles/Titles";
import Show from "../Show/Show";
import {Route} from "react-router-dom";

const Main = props => {
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const fetch = async e => {
        dispatch(inpValue(e.target.value));
        const response = await axiosLink('shows?q=' + e.target.value);
        dispatch(findShows(response.data));
    };

    return (
        <div>
            Search for TV-show
            <div className="inp-box">
                <input type="text" className="inp" onChange={e => fetch(e)} value={state.inputValue} />
                <Titles props={props} />
            </div>
            <Route path={props.match.path + 'show/' + state.id} component={Show}/>
        </div>
    );
};

export default Main;