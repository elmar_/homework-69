import React from 'react';
import {useSelector} from "react-redux";

const Show = () => {
    const show = useSelector(state => state.show);
    const summary = show.summary ? show.summary.replace(/(<([^>]+)>)/gi, '') : 'no summary';
    return (
        <div>
            <h3>{show.name}</h3>
            {<img src={show.image ? show.image['medium'] : '#'} alt={show.name} />}
            <p>{summary}</p>
        </div>
    );
};

export default Show;