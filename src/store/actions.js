export const ADD_SHOW = 'ADD_SHOW';
export const FIND_SHOWS = 'FIND_SHOWS';
export const INP_VALUE = 'INP_VALUE';

export const addShow = (show, id) => ({type: ADD_SHOW, show, id});
export const findShows = id => ({type: FIND_SHOWS, id});
export const inpValue = text => ({type: INP_VALUE, text});
