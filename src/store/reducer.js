import {ADD_SHOW, FIND_SHOWS, INP_VALUE} from "./actions";

const initState = {
    show: {image: '', summary: ''},
    shows: [],
    id: '',
    inputValue: ''
};

const reducer = (state = initState, action) => {
    switch (action.type){
        case ADD_SHOW:
            return {...state, show: action.show, id: action.id, inputValue: action.show.name, shows: []};
        case FIND_SHOWS:
            return {...state, shows: action.id};
        case INP_VALUE:
            return {...state, inputValue: action.text};
        default:
            return state;
    }
};

export default reducer;